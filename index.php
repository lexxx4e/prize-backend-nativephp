<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: x-access-token, content-type");
header("Access-Control-Allow-Methods: GET, POST");

function __autoload($classname) {

    $paths = ["/src/Controller/", "/src/Services/", "/src/Repository/", "/src/Entity/", "/src/Entity/Interface/"];

    foreach ($paths as $path)
    {
        $file = __DIR__ . $path . basename($classname) . ".php";

        if (file_exists($file))
        {
            include_once($file);
        }
    }

}

include_once "./src/Application.php";

$headers = getallheaders();
$request = new \Prize\Service\Request();
$application = new \Prize\Application();
$application->setRequest($request);
$user = null;

if (isset($headers['X-Access-Token']))
{
    $tokenHash = $headers['X-Access-Token'];
}

if (isset($tokenHash))
{
    $token = new \Prize\Service\Token();
    $token->decode($tokenHash);
    $user = $token->getUser();
    $application->setUser($user);
    $application->setToken($token);
}

$route = $request->params();
$rules = explode('/', $route['path']);
$controllerClass = ucfirst($rules[0] . 'Controller');
$controllerClass = sprintf("\\Prize\\Controller\\%s", $controllerClass);
$controller = new $controllerClass();
$operation = $rules[1];

$openRoutes = ['auth'];

if (!method_exists($controller, $operation))
{
    (new \Prize\Service\Response())->write([
        'success' => false,
        'message' => 'Error 404',
        'code' => 404
    ]);

    die;
}

if (!in_array($rules[0], $openRoutes) && !$user instanceof \Prize\Entity\User)
{
    (new \Prize\Service\Response())->write([
        'success' => false,
        'message' => 'No Permission',
        'code' => 403
    ]);

    die;
}

$controller->$operation($application);