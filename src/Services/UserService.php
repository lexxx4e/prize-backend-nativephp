<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 03.11.2018
 * Time: 23:22
 */

namespace Prize\Service;


use Prize\Entity\User;

class UserService
{
    public function encryptPassword($password)
    {
        return md5($password);
    }

    public function create($username, $password)
    {
        $user = new User();

        $user->setUsername($username);

        $user->setPassword($this->encryptPassword($password));

        return $user;
    }
}