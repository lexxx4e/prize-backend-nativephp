<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 04.11.2018
 * Time: 17:35
 */

namespace Prize\Service;


use Prize\Entity\User;

class Token
{
    private $user;

    private $token;

    public function setUser(User $user)
    {
        $this->user = $user;

        $token = JWT::encode($user->toArray(), '');

        $this->token = $token;
    }

    public function encode($token, $key = '')
    {
        $user = new User();

        if (isset($token['username']))
        {
            $user->setUsername($token['username']);
        }

        if (isset($token['password']))
        {
            $user->setPassword($token['password']);
        }

        $this->user = $user;

        $oToken = JWT::encode($token, $key);

        $this->token = $oToken;

        return $oToken;
    }

    public function decode($token, $key = '')
    {
        $this->token = $token;

        $token = JWT::decode($token, $key);

        $token = (array)$token;

        $user = new User();

        if (isset($token['username']))
        {
            $user->setUsername($token['username']);
        }

        if (isset($token['password']))
        {
            $user->setPassword($token['password']);
        }

        $this->user = $user;

        return $token;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function getToken()
    {
        return $this->token;
    }

}