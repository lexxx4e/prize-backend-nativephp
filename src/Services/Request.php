<?php

/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 03.11.2018
 * Time: 22:48
 */

namespace Prize\Service;

class Request
{
    public function get($url, array $params = [])
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, sprintf("%s?%s", $url, http_build_query($params)));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        $out = curl_exec($curl);
        curl_close($curl);

        return $out;
    }

    public function post($url, array $params = [])
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($params));
        $out = curl_exec($curl);
        curl_close($curl);

        return $out;
    }

    public function params()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $body = @file_get_contents('php://input');

            return [
                'body' => $body,
                'path' => $_REQUEST['path']
            ];
        }

        return $_REQUEST;
    }

    public function body()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            $result =  $_POST;

            $result = (empty($result)) ? json_decode(@file_get_contents('php://input'), true) : $result;

            return $result;
        }

        return null;
    }


}