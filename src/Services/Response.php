<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 03.11.2018
 * Time: 23:24
 */

namespace Prize\Service;


class Response
{
    public function write(array $data)
    {
        header('Content-Type: application/json');

        die(json_encode($data));
    }
}