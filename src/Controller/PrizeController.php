<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 04.11.2018
 * Time: 0:02
 */

namespace Prize\Controller;

use Prize\Application;
use Prize\Entity\BonusPrize;
use Prize\Entity\MoneyPrize;
use Prize\Entity\Prize;
use Prize\Entity\User;
use Prize\Repository\PrizeRepository;
use Prize\Service\Response;
use Prize\Service\Token;

class PrizeController
{
    private $prizeRepository;

    public function __construct()
    {
        $this->prizeRepository = new PrizeRepository();
    }

    public function create(Application $application)
    {
        try
        {
            /**
             * @var Token $token
             */
            $token = $application->getToken();

            /**
             * @var User $user
             */
            $user = $token->getUser();

            $prize = $this->prizeRepository->create4user($user);

            (new Response())->write([
                'success' => true,
                'prize' => $prize
            ]);
        }
        catch (\Exception $e)
        {
            (new Response())->write([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

    }

    public function convert(Application $application)
    {
        try
        {
            $params = $application->getRequest()->params();

            /**
             * @var Token $token
             */
            $token = $application->getToken();

            /**
             * @var User $user
             */
            $user = $token->getUser();

            $prize = json_decode($params['body'], true)['prize'];

            $id = $prize['id'];

            if ($prize['type'] == 'bonus')
            {
                /**
                 * @var BonusPrize $prize
                 */
                $prize = (new PrizeRepository())->getPrizeByArray($prize);
                $prize = $prize->convertToMoney();
                $prize->setUser($user);
                $prize->setId($id);
                $prize = (new PrizeRepository())->getArrayByPrize($prize);
            }

            (new Response())->write([
                'success' => true,
                'prize' => $prize
            ]);
        }
        catch (\Exception $e)
        {
            (new Response())->write([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function save(Application $application)
    {
        try
        {
            $params = $application->getRequest()->params();

            /**
             * @var Token $token
             */
            $token = $application->getToken();

            /**
             * @var User $user
             */
            $user = $token->getUser();

            $prize = json_decode($params['body'], true)['prize'];

            $prize['username'] = $user->getUsername();

            $this->prizeRepository->save($prize);

            (new Response())->write([
                'success' => true
            ]);
        }
        catch (\Exception $e)
        {
            (new Response())->write([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function all(Application $application)
    {
        try
        {
            /**
             * @var Token $token
             */
            $token = $application->getToken();

            /**
             * @var User $user
             */
            $user = $token->getUser();

            $prizes = $this->prizeRepository->findAllPrizesByUsername($user->getUsername());

            (new Response())->write([
                'success' => true,
                'prizes' => $prizes
            ]);
        }
        catch (\Exception $e)
        {
            (new Response())->write([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

    }

    public function send(Application $application)
    {
        try
        {
            $params = $application->getRequest()->params();
            //throw new \Exception(print_r($params, true));
            $prizeId = json_decode($params['body'], true)['prize_id'];

            /**
             * @var Prize $prize
             */
            $prize = $this->prizeRepository->findById($prizeId);

            /**
             * @var User $user
             */
            $user = $application->getUser();

//            if ($prize->getUser()->getUsername() !== $user->getUsername())
//            {
//                throw new \Exception("No Permission", 403);
//            }

            $prize->setUser($user);

            $prize->send();

            (new PrizeRepository())->remove($prize->getId());

            (new Response())->write([
                'success' => true
            ]);
        }
        catch (\Exception $e)
        {
            (new Response())->write([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

    }

}