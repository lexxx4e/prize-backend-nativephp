<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 03.11.2018
 * Time: 22:54
 */

namespace Prize\Controller;

use Prize\Application;
use Prize\Entity\User;
use Prize\Repository\UserRepository;
use Prize\Service\Response;
use Prize\Service\Token;
use Prize\Service\UserService;

class AuthController
{
    private $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    public function login(Application $application)
    {
        try
        {
            $params = $application->getRequest()->body();

            if (isset($params['username']) && isset($params['password']))
            {
                $user = $this->userRepository->findByUsername($params['username']);

                if (!$user instanceof User)
                {
                    throw new \Exception("User No Exists");
                }

                if ($user->getPassword() == (new UserService())->encryptPassword($params['password']))
                {
                    $token = new Token();

                    $token->setUser($user);

                    (new Response())->write([
                        'success' => true,
                        'token' => $token->getToken()
                    ]);
                }
                else
                {
                    throw new \Exception("Incorrect Password");
                }
            }
            else
            {
                throw new \Exception("Incorrect Input Data");
            }
        }
        catch (\Exception $e)
        {
            (new Response())->write([
                'success' => false,
                'message' => $e->getMessage(),
                'code' => $e->getCode()
            ]);
        }

    }
}