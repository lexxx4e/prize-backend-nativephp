<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 05.11.2018
 * Time: 22:02
 */

namespace Prize\Controller;


use Prize\Application;
use Prize\Entity\User;
use Prize\Repository\UserRepository;
use Prize\Service\Response;
use Prize\Service\Token;

class UserController
{
    public function info(Application $application)
    {
        try
        {
            /**
             * @var Token $token
             */
            $token = $application->getToken();

            /**
             * @var User $user
             */
            $user = $token->getUser();

            $info = (new UserRepository())->getUserInfo($user->getUsername());

            (new Response())->write([
                'success' => true,
                'info' => $info
            ]);
        }
        catch (\Exception $e)
        {
            (new Response())->write([
                'success' => false,
                'prize' => $e->getMessage()
            ]);
        }

    }
}