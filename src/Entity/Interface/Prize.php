<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 03.11.2018
 * Time: 13:43
 */

namespace Prize\Entity\Impl;

interface Prize {

    public function send();

    public function create();

}