<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 03.11.2018
 * Time: 13:47
 */

namespace Prize\Entity;


class ToyPrize extends Prize
{
    const PATH_FILE_TOYS_REPOSITORY = __DIR__ . '/../../data/toys.json';

    public $type = 'toy';

    private $arToyPics = [];

    public function __construct()
    {
        parent::__construct();

        $jsToys = file_get_contents(self::PATH_FILE_TOYS_REPOSITORY);

        $this->arToyPics = json_decode($jsToys, true);
    }

    public function send()
    {
        // Sending Letter
        sleep(3);
    }

    public function create()
    {
        $this->value = $this->calc();

        return $this;
    }

    protected function calc()
    {
        return $this->arToyPics[rand(0, sizeof($this->arToyPics) - 1)];
    }

}