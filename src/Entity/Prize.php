<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 03.11.2018
 * Time: 13:39
 */

namespace Prize\Entity;

use \Prize\Entity\Impl\Prize as PrizeImpl;

abstract class Prize implements PrizeImpl {

    public $id;

    public $value;

    public $type;

    private $user;

    public function __construct()
    {
        $this->setId(md5(time() . rand(10000, 99999)));
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }



    abstract public function send();

    abstract protected function calc();

}