<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 03.11.2018
 * Time: 13:52
 */

namespace Prize\Entity;


class BonusPrize extends Prize
{
    public $type = 'bonus';

    const MAX_BONUS = 10;

    const MIN_BONUS = 1;

    const COEF_CHANGING = 12;

    public function send()
    {
        // Convert Bonus To Money
        // Make Transaction By Bank API
        sleep(2);
    }

    public function create()
    {
        $this->value = $this->calc();

        return $this;
    }

    protected function calc()
    {
        return rand(self::MIN_BONUS, self::MAX_BONUS);
    }


    public function convertToMoney()
    {
        $prize = new MoneyPrize();
        $prize->setPrice($this->value * self::COEF_CHANGING);

        return $prize;
    }
}