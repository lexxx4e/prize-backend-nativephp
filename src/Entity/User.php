<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 04.11.2018
 * Time: 0:11
 */

namespace Prize\Entity;


class User
{
    private $username;

    private $password;

    private $bonus = 0;

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getEmail()
    {
        return $this->getUsername();
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function toArray()
    {
        return [
            'username' => $this->getUsername(),
            'password' => $this->getPassword()
        ];
    }

    public function toString()
    {
        return json_encode($this->toArray());
    }

    /**
     * @return int
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * @param int $bonus
     */
    public function setBonus($bonus)
    {
        $this->bonus = $bonus;
    }



}