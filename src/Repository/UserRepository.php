<?php

/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 03.11.2018
 * Time: 23:08
 */

namespace Prize\Repository;

use Prize\Entity\Prize;
use Prize\Entity\User;

class UserRepository
{
    const PATH_FILE_USERS_REPOSITORY = __DIR__ . '/../../data/users.json';

    /**
     * @param $username
     * @return null|User
     */
    public function findByUsername($username)
    {
        $jsUsers = file_get_contents(self::PATH_FILE_USERS_REPOSITORY);

        $arUsers = json_decode($jsUsers, true);

        foreach ($arUsers as $u)
        {
            if ($u['username'] == $username)
            {
                $user = new User();

                $user->setUsername($u['username']);

                $user->setPassword($u['password']);

                return $user;
            }
        }

        return null;
    }

    public function getUserInfo($username)
    {
        $prizes = (new PrizeRepository())->findAllPrizesByUsername($username);

        $arResult = [
            'bonus' => 0,
            'money' => 0
        ];

        /**
         * @var Prize $prize
         */
        foreach ($prizes as $prize)
        {
            if (in_array($prize['type'], ['money', 'bonus']))
            {
                $arResult[$prize['type']] += $prize['value'];
            }
        }

        return $arResult;
    }
}