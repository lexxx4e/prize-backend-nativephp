<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 03.11.2018
 * Time: 23:30
 */

namespace Prize\Repository;


use Prize\Entity\BonusPrize;
use Prize\Entity\MoneyPrize;
use Prize\Entity\Prize;
use Prize\Entity\ToyPrize;
use Prize\Entity\User;

class PrizeRepository
{
    const PATH_FILE_PRIZES_REPOSITORY = __DIR__ . '/../../data/prizes.json';

    public function findAllPrizesByUsername($username)
    {
        $arResult = [];

        $jsPrizes = file_get_contents(self::PATH_FILE_PRIZES_REPOSITORY);

        $arPrizes = json_decode($jsPrizes, true);

        foreach ($arPrizes as $prize)
        {
            if ($prize['username'] === $username)
            {
                $arResult[] = $prize;
            }
        }

        return $arResult;
    }

    public function findById($prizeId)
    {
        $jsPrizes = file_get_contents(self::PATH_FILE_PRIZES_REPOSITORY);

        $arPrizes = json_decode($jsPrizes, true);

        foreach ($arPrizes as $prize)
        {
            if ($prize['id'] === $prizeId)
            {
                switch ($prize['type'])
                {
                    case 'toy':
                        $oPrize = new ToyPrize();
                        break;
                    case 'money':
                        $oPrize = new MoneyPrize();
                        break;
                    case 'bonus':
                        $oPrize = new BonusPrize();
                        break;
                    default:
                        throw new \Exception("Incorrect Prize Type");
                        break;
                }

                $oPrize->setId($prize['id']);
                $oPrize->setValue($prize['value']);
                $oPrize->setType($prize['type']);

                return $oPrize;
            }
        }

        return null;
    }

    public function remove($prizeId)
    {
        $jsPrizes = file_get_contents(self::PATH_FILE_PRIZES_REPOSITORY);

        $arPrizes = json_decode($jsPrizes, true);

        $resPrizes = [];

        foreach ($arPrizes as $prize)
        {
            if ($prize['id'] !== $prizeId)
            {
                $resPrizes[] = $prize;
            }
        }

        file_put_contents(self::PATH_FILE_PRIZES_REPOSITORY, json_encode($resPrizes));
    }

    public function save($prize)
    {
        try
        {
            $jsPrizes = file_get_contents(self::PATH_FILE_PRIZES_REPOSITORY);

            $arPrizes = json_decode($jsPrizes, true);

            $arPrizes[] = $prize;

            file_put_contents(self::PATH_FILE_PRIZES_REPOSITORY, json_encode($arPrizes));

            return true;
        }
        catch (\Exception $e)
        {
            throw $e;
        }
    }

    public function create4user(User $user)
    {
        $prizeClasses = [BonusPrize::class, MoneyPrize::class, ToyPrize::class];

        $randPrizeClass = $prizeClasses[rand(0, sizeof($prizeClasses) - 1)];

        /**
         * @var Prize $prize
         */
        $prize = new $randPrizeClass;

        $prize->setUser($user);

        $prize->create();

        return $prize;
    }

    public function getPrizeByArray($arPrize)
    {
        if (!isset($arPrize['type']) || !in_array($arPrize['type'], ['money', 'bonus', 'toy']))
        {
            throw new \Exception("Incorrect Prize Type");
        }

        if (!isset($arPrize['value']))
        {
            throw new \Exception("Incorrect Prize Value");
        }

        $entityClass = ucfirst($arPrize['type']);
        $entityClass = sprintf("\\Prize\\Entity\\%sPrize", $entityClass);

        /**
         * @var Prize $prize
         */
        $prize = new $entityClass();

        $prize->setValue($arPrize['value']);

        return $prize;
    }

    public function getArrayByPrize(Prize $prize)
    {
        return [
            "id" => $prize->getId(),
            "type" => $prize->getType(),
            "value" => $prize->getValue()
        ];
    }
}